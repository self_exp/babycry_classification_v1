# the image size that we want to reshape the images to
IMG_SIZE = 224

DATA_DIR_PATH = "../../data/"
# filepath of EntireSpectrogramDataset (ESD)
TRAINING_DIR_NAME = DATA_DIR_PATH + "Dataset/ESD80"
TEST_DIR_NAME = DATA_DIR_PATH + "Dataset/ESDVal"

MODEL_PERSIST_PATH = "../../models/"

MODEL_NAME_SVM="SVM"
MODEL_NAME_TL_RESNET50="transfer_learning_resnet50"
