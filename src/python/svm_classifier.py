from sklearn.model_selection import cross_val_score, train_test_split
from sklearn import svm
import time
import data_loader as dl
import constants as cs
import ml_utils as mu


def get_SVM_model(features, labels):
    ##Train model
    Start = time.time()
    SVM = svm.SVC(gamma='auto', C=100, probability=True, shrinking=True, verbose=True)
    print("complete SVM")
    xTrain, xTest, yTrain, yTest = train_test_split(features, labels, test_size=0.2, shuffle=True)
    print("complete Split")
    score = cross_val_score(SVM, features, labels, cv=5, verbose=2, n_jobs=None)
    SVM.fit(xTrain, yTrain)
    print("complete fit")
    End = time.time()

    print("Time taken to train SVM: ", End - Start)

    return SVM


def run_svm_model():
    # Load training data
    features, labels = dl.get_data_features_label(is_training_dataset=True, convert_image_to_1D=False)
    test_features, test_labels = dl.get_data_features_label(is_training_dataset=False, convert_image_to_1D=True)

    model = get_SVM_model(features, labels)
    y_pred = model.predict(test_features)

    mu.get_model_prediction_performance(y_actual=test_labels, y_pred=y_pred)
    #saving model
    mu.persist_model(cs.MODEL_NAME_SVM)

    return


if __name__ == '__main__':
    run_svm_model()
