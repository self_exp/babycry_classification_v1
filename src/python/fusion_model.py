import numpy as np
from tqdm import tqdm # for progress bar
from sklearn.externals import joblib

import data_loader as dl
import svm_classifier as svm_cl
import transer_learning_resnet50 as tl_rs50
import ml_utils as mu
import constants as cs


class DecisionFusion(object):
    def __init__(self, svm_model_name, tl_resnet50_model_name):
        self.SVM = None
        self.TL_Resnet50 = None
        self.load_models(svm_model_name, tl_resnet50_model_name)

    def load_models(self, svm_model_name, tl_resnet50_model_name):
        # Load training data
        train_features, train_labels = dl.get_data_features_label(is_training_dataset=True, convert_image_to_1D=False)
        svm_features = dl.convert_features_to_1D(train_features)

        try:
            self.SVM = mu.load_persisted_model(svm_model_name)
        except:
            self.SVM = svm_cl.get_SVM_model(svm_features, train_labels)

        try:
            self.TL_Resnet50 = mu.load_persisted_model(tl_resnet50_model_name)
        except:
            self.TL_Resnet50 = tl_rs50.get_tl_resnet50_model(train_features, train_labels)

        return

    def predict(self, test_features):
        svm_test_features = dl.convert_features_to_1D(test_features)
        # contains the predictions for the test following the decision fusion process
        predictions = []
        for i in tqdm(range(len(test_features))):
            # seperates the predictions for the ith test image [pred1.pred2,pred3,pred4,pred5] as a list
            SVMpredictions = (self.SVM.predict_proba(svm_test_features[[i]])[0]).tolist()
            Nrespredictions = (self.TL_Resnet50.predict(test_features[[i]])[0]).tolist()

            # stores the results of the decision fusion calculation for each index in the prediction arrays
            tempArray = []
            for j in range(len(SVMpredictions)):
                # [a1,a2,a3,a4,a5] [b1,b2,b3,b4,b5] --> a1 + b1 = total
                total = SVMpredictions[j] + Nrespredictions[j]
                # total = total / number of models we are using for the prediction
                result = total / 2.0
                # store the results of index 1
                tempArray.append(result)
                # proceed to next index until end of index = # of classes available

            # prints the results of the decision fusion for all of the classes
            print("tempArray: ", tempArray)
            # returns that index of the maximum value in the temp array
            pred = np.argmax(tempArray)
            # stores the index of the maximum value into a predictions array
            predictions.append(pred)
            # prints the updated predictions array for each test image until end of test image directory
            print("predictions: ", predictions)

            return predictions


def run_fusion_model():
    test_features, test_labels = dl.get_data_features_label(is_training_dataset=False, convert_image_to_1D=False)
    model = DecisionFusion()
    y_pred = model.predict(test_features)

    print("\nDecisionFusion")
    mu.get_model_prediction_performance(y_actual=test_labels, y_pred=y_pred)

    print("\nSVM")
    svm_test_features = dl.convert_features_to_1D(test_features)
    mu.get_model_prediction_performance(y_actual=test_labels, y_pred=model.SVM.predict(svm_test_features))

    print("\nTL_Resnet50")
    mu.get_model_prediction_performance(y_actual=test_labels, y_pred=model.TL_Resnet50.predict(test_features.tolist().squeeze()))
    return


if __name__ == '__main__':
    run_fusion_model()
