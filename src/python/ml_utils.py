import os
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from tqdm import tqdm


def get_model_prediction_performance(y_actual, y_pred):
    accuracy = accuracy_score(y_actual, y_pred)
    p1, r1, f1, junk = precision_recall_fscore_support(y_actual, y_pred, average='macro')
    p2, r2, f2, junk = precision_recall_fscore_support(y_actual, y_pred, average='micro')
    p3, r3, f3, junk = precision_recall_fscore_support(y_actual, y_pred, average='weighted')

    print("Prediction Details: \n", count_predictions(y_actual, y_pred))
    print("Accuracy: ", accuracy)
    print("Macro average: \n\t\tPrecision: %3.3f, Recall: %3.3f, F1-score: %3.3f" % (p1, r1, f1))
    print("Micro average: \n\t\tPrecision: %3.3f, Recall: %3.3f, F1-score: %3.3f" % (p2, r2, f2))
    print("Weighted average: \n\t\tPrecision: %3.3f, Recall: %3.3f, F1-score: %3.3f" % (p3, r3, f3))

    confusion_matrix(y_actual, y_pred)

    return

def count_predictions(y_actual, y_pred):
    correct = 0
    incorrect = 0
    for i in tqdm(range(len(y_actual))):
        if (y_pred[i] != y_actual[i]):
            incorrect += 1
        else:
            correct += 1
    return ("correct: ", correct, " incorrect: ", incorrect, "prediction: ", (correct / len(y_actual)))


def persist_model(model, model_name):
    from joblib import dump
    if not os.path.exists('my_folder'): os.makedir(MODEL_PERSIST_PATH)
    dump(model, cs.MODEL_PERSIST_PATH+model_name+'.joblib')

def load_persisted_model(model_name) :
    from joblib import load
    load(cs.MODEL_PERSIST_PATH+model_name+'.joblib')


if __name__ == '__main__':
    y_pred = [0, 2, 1, 3]
    y_actual = [0, 1, 2, 3]
    get_model_prediction_performance(y_actual, y_pred)

