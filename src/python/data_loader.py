import matplotlib
import random
import os
import time
import numpy as np
import tensorflow as tf
import datetime, os
from tqdm import tqdm as t
import cv2
import constants as cs


def load_image_data(data_dir):
    # labels
    sounds = ['Asphyxiate', 'Deaf', 'Hunger', 'Normal', 'Pain']
    # used to store the tuples that pair each image with their related label
    training_data = []

    # sorts through each folder (different label name)
    for sound in sounds:
        # specifies path of each folder
        path = os.path.join(data_dir, sound)
        index = sounds.index(sound)
        # used to store the number of corrupt images that were detected during processing
        count = 0

        # processes each image in a given folder
        for img in t(os.listdir(path)):
            # try catch used to record and bypass over corrupt images
            try:
                # comverts image file to an  array
                img_toArray = cv2.imread(os.path.join(path, img))
                # resizing the images to an appropriate size
                new_array = cv2.resize(img_toArray, (cs.IMG_SIZE, cs.IMG_SIZE))
                # append tuple contain (feature (image array),label)
                training_data.append([new_array, index])
            except Exception as e:
                # record that there was a corrupt image
                count += 1
                # bypass over that image
                pass
    # shuffle (feature,label) sets
    random.shuffle(training_data)
    return training_data


def get_data_features_label(is_training_dataset, convert_image_to_1D=False):
    data_dir = cs.TRAINING_DIR_NAME if is_training_dataset else cs.TEST_DIR_NAME

    print("cs.IMG_SIZE: ", cs.IMG_SIZE)
    Start = time.time()
    # run our model through the method
    trainingImages = load_image_data(data_dir)

    # store the image arrays
    features = []
    # stores the related labels
    labels = []
    # seperates the features from the labels in the tuple generated from the method
    for feature, label in t(trainingImages):
        features.append(feature)
        labels.append(label)
    # reshape the features array to be (number of elements being considered, img_size * img_size * num_of_color_channels(3))
    # as a numpy array for easier computation and the shape method
    features = convert_features_to_1D(features) if convert_image_to_1D else features

    # convert the labels array to a numpy array for easier computation and the shape method
    # features = np.asarray(features)
    labels = np.asarray(labels)
    features = np.asarray(features)
    features = features / 255

    print("features.shape: ", features.shape)
    print("labels.shape: ", labels.shape)
    END = time.time()

    print("Amount of processing time (in seconds): %3.4f", (END - Start))

    return features, labels


def convert_features_to_1D(features):
    return np.array(features).reshape(len(features), cs.IMG_SIZE * cs.IMG_SIZE * 3)


if __name__ == '__main__':
    features, labels= get_data_features_label(is_training_dataset=False)
    convert_features_to_1D(features)
    print(len(features))