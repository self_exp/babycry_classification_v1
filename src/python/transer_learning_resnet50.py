import tensorflow as tf
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

from keras.models import Model
from keras.optimizers import Adam, SGD
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import ResNet50
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation, Dropout, GlobalAveragePooling2D
from keras.preprocessing import image
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import train_test_split

import data_loader as dl
import constants as cs
import ml_utils as mu


def get_tl_resnet50_model(features, labels):
    # First Keras tutorial
    res_model = ResNet50(weights='imagenet', include_top=False)  # imports the mobilenet model and discards the last 1000 neuron layer.
    x = res_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)  # we add dense layers so that the model can learn more complex functions and classify for better results.
    x = Dense(1024, activation='relu')(x)  # dense layer 2
    x = Dropout(0.25)(x)
    x = Dense(512, activation='relu')(x)  # dense layer 3
    preds = Dense(5, activation='softmax')(x)  # final layer with softmax activation

    # specify the inputs and outputs
    # now a model has been created based on our architecture
    nres_model = Model(inputs=res_model.input, outputs=preds)

    # Adam optimizer
    # loss function will be categorical cross entropy
    # evaluation metric will be accuracy
    adam = Adam(lr=0.0001)
    nres_model.compile(optimizer=adam, loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    nres_model.fit(features, labels, batch_size=50, epochs=10, validation_data=(Testfeatures, Testlabels))

    return nres_model


def run_tl_resnet50_model():
    # Load training data
    features, labels = dl.get_data_features_label(is_training_dataset=True, convert_image_to_1D=False)

    model = get_tl_resnet50_model(features, labels)

    # Test on valudation data
    test_features, test_labels = dl.get_data_features_label(is_training_dataset=False, convert_image_to_1D=False)
    y_pred = model.predict(test_features)

    mu.get_model_prediction_performance(y_actual=test_labels, y_pred=y_pred)

    #saving model
    mu.persist_model(cs.MODEL_NAME_TL_RESNET50)

    return


if __name__ == '__main__':
    run_tl_resnet50_model()
