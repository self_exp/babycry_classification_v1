# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Has code related to multclass baby cry classification
* Version

### How do I get set up? ###

* Summary of set up
* Configuration 
* Dependencies 
	python3,
	pip3 install opencv-python
	pip3 install keras
	pip3 install tqdm
	
	python scikit, numpy library

* Data : Download Dataset.zip from our lab repo into data folder of this package and unzip it. 
* How to run tests
* Deployment instructions

### Contribution guidelines ###
The code in initial version was arranged and structured from .ipynb files written by REU 2019 students Lillian and Nadim.  
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Sunitha Basodi (organized and documented)